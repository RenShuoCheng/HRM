﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using HRMSModel;
using System.Configuration;
using Newtonsoft.Json;
namespace HRMSDAL
{
    public class Logindal
    {
        public int Login(UsersLoginM m)
        {
            DBhelper db = new DBhelper();
            object i;
            return  db.Login("proc_Login","@uYHM", m.uYHM, "@uPWD", m.uPWD,out i);
        }
    }
    public class staffDAL
    {
        public List<staffM> show()
        {
            List<staffM> list = null;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
            {
                SqlCommand com = new SqlCommand();
                com.CommandText = "proc_staffshow";
                com.Connection = conn;
                com.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataAdapter dap = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                dap.Fill(dt);
                list=JsonConvert.DeserializeObject<List<staffM>>(JsonConvert.SerializeObject(dt));
            }
            return list;
        }
    }
    public class DBhelper
    {
        private string configuration = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        /// <summary>
        /// 登录的数据查询
        /// </summary>
        /// <param name="comtext"></param>
        /// <param name="ParameterName1"></param>
        /// <param name="value1"></param>
        /// <param name="ParameterName2"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        public int Login(string comtext,string ParameterName1,object value1, string ParameterName2, object value2,out object i)
        {
            using (SqlConnection conn = new SqlConnection(configuration))
            {
                SqlCommand com = new SqlCommand();
                com.Connection = conn;
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = comtext;
                SqlParameter[] par = {
                    new SqlParameter(ParameterName1,value1),
                    new SqlParameter (ParameterName2,value2),
                };
                com.Parameters.AddRange(par);
                conn.Open();
               
                int j= com.ExecuteNonQuery();
                if (j == 1)
                {
                    i = com.ExecuteScalar();
                    return 1;
                }
                else
                {
                    i = 0;
                    return 0;
                }
            }
        }
    }
}
