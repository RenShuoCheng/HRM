﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMSServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpPost]
        public int Yhm(string uYhm)
        {
            int i = 0;
            Session["yhm"] = uYhm;
            return i;
        }
        [HttpGet]
        public string Showyhm()
        {
            object o = Session["yhm"];
            if (string.IsNullOrWhiteSpace(o.ToString()))
            {
                return o.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}
